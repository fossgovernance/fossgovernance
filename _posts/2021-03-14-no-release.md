---

title: "No release for 2021-03"
date: 2021-03-14
author: vmbrasseur
excerpt: "No release for 2021-03"
layout: single
permalink: /2021/03/no-release

---
Hey there, everyone!

There won't be a release this month. Your friendly neighbourhood maintainer has some higher priority things to focus on this weekend.

While you're waiting for her to resolve that stuff, how about [suggesting](/contribute/) some new documents for the collection? She'll add them in April. 😊
