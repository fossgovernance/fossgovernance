---

title: "2023-Q1 Release Announcement"
date: 2023-03-26
author: vmbrasseur
excerpt: "Announcing the 2023-Q1 release of the FOSS Governance Collection"
layout: single

---
Hi, all. Long time no see!

We're now aiming to release quarterly, since monthly was obviously a bridge too far for us.

This quarter brings **40** new documents to the [collection](https://www.zotero.org/groups/2310183/foss_governance/items). 

We've also created new categories for the documents. You can view and search the collection either in its entirety or limited to one of:

* For Profit
* Non-Profit
* Project

Please have a look at [the release notes](https://fossgovernance.org/release-notes/#2023-q1) for complete details and as always, [keep those suggestions coming](https://fossgovernance.org/contribute/#ways-to-contribute)! The more documents we add to the collection, the more useful it is for all of us.
